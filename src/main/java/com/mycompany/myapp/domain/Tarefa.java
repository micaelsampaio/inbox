package com.mycompany.myapp.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Tarefa.
 */
@Entity
@Table(name = "tarefa")
public class Tarefa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "descricao", nullable = false)
    private String descricao;

    @NotNull
    @Column(name = "id_contacto", nullable = false)
    private Integer idContacto;

    @NotNull
    @Column(name = "nome_contacto", nullable = false)
    private String nomeContacto;

    @NotNull
    @Column(name = "email_contacto", nullable = false)
    private String emailContacto;

    @NotNull
    @Column(name = "rejeitada", nullable = false)
    private Boolean rejeitada;

    @NotNull
    @Column(name = "efectuada", nullable = false)
    private Boolean efectuada;

    @NotNull
    @Column(name = "uniqid", nullable = false)
    private String uniqid;



    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public Tarefa descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getIdContacto() {
        return idContacto;
    }

    public Tarefa idContacto(Integer idContacto) {
        this.idContacto = idContacto;
        return this;
    }

    public void setIdContacto(Integer idContacto) {
        this.idContacto = idContacto;
    }

    public String getNomeContacto() {
        return nomeContacto;
    }

    public Tarefa nomeContacto(String nomeContacto) {
        this.nomeContacto = nomeContacto;
        return this;
    }

    public void setNomeContacto(String nomeContacto) {
        this.nomeContacto = nomeContacto;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public Tarefa emailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
        return this;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public Boolean isRejeitada() {
        return rejeitada;
    }

    public Tarefa rejeitada(Boolean rejeitada) {
        this.rejeitada = rejeitada;
        return this;
    }

    public void setRejeitada(Boolean rejeitada) {
        this.rejeitada = rejeitada;
    }

    public Boolean isEfectuada() {
        return efectuada;
    }

    public Tarefa efectuada(Boolean efectuada) {
        this.efectuada = efectuada;
        return this;
    }

    public void setEfectuada(Boolean efectuada) {
        this.efectuada = efectuada;
    }

    public String getUniqid() {
        return uniqid;
    }

    public Tarefa uniqid(String uniqid) {
        this.uniqid = uniqid;
        return this;
    }

    public void setUniqid(String uniqid) {
        this.uniqid = uniqid;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tarefa tarefa = (Tarefa) o;
        if (tarefa.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tarefa.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tarefa{" +
            "id=" + getId() +
            ", descricao='" + getDescricao() + "'" +
            ", idContacto=" + getIdContacto() +
            ", nomeContacto='" + getNomeContacto() + "'" +
            ", emailContacto='" + getEmailContacto() + "'" +
            ", rejeitada='" + isRejeitada() + "'" +
            ", efectuada='" + isEfectuada() + "'" +
            ", uniqid='" + getUniqid() + "'" +
            "}";
    }
}
