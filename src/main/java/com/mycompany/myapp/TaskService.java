package com.mycompany.myapp;

import com.mycompany.myapp.domain.Tarefa;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;
import com.mycompany.myapp.repository.TarefaRepository;

@Service
public class TaskService {

    private final TarefaRepository tarefaRepository;

    public TaskService(TarefaRepository tarefaRepository) {
        this.tarefaRepository = tarefaRepository;
    }

    @StreamListener(ConsumerChannel.CHANNEL)
    public void consume(Tarefa tarefa) {

        System.out.println("__________________________________________");
        System.out.println(tarefa);

        Tarefa result = tarefaRepository.save(tarefa);
        System.out.println(result);
    }
}